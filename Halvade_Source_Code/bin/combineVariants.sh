#!/bin/bash

DIR=$1
echo "reading from $DIR"
REF=$2
VARIANTS=""

for vcf in $DIR/*.vcf
do
#	echo $vcf
	VARIANTS="$VARIANTS --variant $vcf";
	# fixme if more then 250 split up!
done
#echo $VARIANTS;

MEM=-Xmx2g
GATK=GenomeAnalysisTK.jar
THREADS=2

java $MEM -jar $GATK -T CombineVariants -nt $THREADS -R $REF -o combined.vcf -sites_only -genotypeMergeOptions UNIQUIFY $VARIANTS

