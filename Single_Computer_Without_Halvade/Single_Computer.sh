#!/bin/bash
# Author: Georgios Giatrakis
# Student number: 920118262050
# The script runs the GATK pipeline without Recalibration step. It was
# developed for comparison purposes with Halvade framework running in a 
# physical cluster 

# Usage: bash Single_Computer.sh <fastq_0.fastq> <fastq_1.fastq>
#  <Reference_file.fasta> <number_of_cores>
# Input:
# 	 fastq_0.fastq: fastq file, the first of the two input fastq Files
# 	 fastq_1.fastq: fastq file, the second of the two input fastq Files
# 	 Reference_file.fasta: fasta file of the reference genome
# 	 number_of_cores: Integer, number of cores to be used
# Out: 
# 	Output: folder with variant calling, index and metrics file


#Set Params via Command Line
fastq1=$1
fastq2=$2
ref_genome=$3
nThreads=$4

mkdir Output
mkdir tmp

echo =================================================================================================================
echo
echo ---------------------------------------START BWA INDEX --------------------------------------------------------
echo
echo =================================================================================================================

# Indexing reference file 
tools/bwa index $ref_genome
tools/samtools faidx $ref_genome
java -jar tools/picard.jar CreateSequenceDictionary REFERENCE=$ref_genome OUTPUT=$genome_name.dict


echo =================================================================================================================
echo
echo ---------------------------------------START BWA ALIGNMENT --------------------------------------------------------
echo
echo =================================================================================================================

#Align with bwa
tools/bwa mem -M -R '@RG\tID:sample_1\tLB:sample_1\tPL:ILLUMINA\tPM:HISEQ\tSM:sample_1' $ref_genome $fastq1 $fastq2 -t $nThreads  > aligned_reads.sam


echo =================================================================================================================
echo
echo ---------------------------------------SORT SAM BY COORDINATE ---------------------------------------------------
echo
echo =================================================================================================================

# Sort sam by coordinate
java -Djava.io.tmpdir=`pwd`/tmp -jar tools/picard.jar SortSam INPUT=aligned_reads.sam OUTPUT=sorted_reads.bam SORT_ORDER=coordinate TMP_DIR=`pwd`/tmp


echo =================================================================================================================
echo
echo ---------------------------------------Mark Duplicates --------------------------------------------------------
echo
echo =================================================================================================================

# Mark Duplicates
java -jar tools/picard.jar MarkDuplicates INPUT=sorted_reads.bam OUTPUT=dedup_reads.bam METRICS_FILE=Output/metrics.txt ASSUME_SORTED=true VALIDATION_STRINGENCY=LENIENT


echo =================================================================================================================
echo
echo ---------------------------------------Build Bam Index --------------------------------------------------------
echo
echo =================================================================================================================

# Build BAM Index
java -jar tools/picard.jar BuildBamIndex INPUT=dedup_reads.bam #This goes as input to HaplotypeCaller in Halvade


echo =================================================================================================================
echo
echo ---------------------------------------Call Variants --------------------------------------------------------
echo
echo =================================================================================================================

# Call Variants 
java -jar tools/GenomeAnalysisTK.jar -T HaplotypeCaller -nct $nThreads -R $ref_genome -I dedup_reads.bam -o Output/raw_variants.vcf


# Delete Intermediate Files
rm aligned_reads.sam
rm sorted_reads.bam
rm dedup_reads.bam
rm dedup_reads.bai
rm realignment_targets.list
rm realigned_reads.bam
rm realigned_reads.bai


echo =================================================================================================================
echo
echo ---------------------------------------JOB DONE------------------------------------------------------------------
echo
echo =================================================================================================================


