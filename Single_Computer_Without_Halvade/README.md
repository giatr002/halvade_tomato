Author: Georgios Giatrakis

Student number: 920118262050

Description:
The script runs the GATK pipeline without Recalibration step. It was
developed for comparison purposes with Halvade framework running in a 
physical cluster 

Usage: bash Single_Computer.sh <fastq_0.fastq> <fastq_1.fastq>
  <Reference_file.fasta> <number_of_cores>
  
Input:

 	 fastq_0.fastq: fastq file, the first of the two input fastq Files
 	 fastq_1.fastq: fastq file, the second of the two input fastq Files
 	 Reference_file.fasta: fasta file of the reference genome
     number_of_cores: Integer, number of cores to be used

Output: 

     folder with variant calling, index and metrics file