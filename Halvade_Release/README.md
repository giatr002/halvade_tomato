# Halvade_Tomato

A slightly modified version of Halvade 1.3.0. It is modified to run in Amazon EMR on tomato genome out of the box 

To run it, download this release and follow this guide:
https://git.wur.nl/giatr002/halvade_tomato/wikis/Appendix-A.-Recipe:-Variant-Calling-of-tomato-data-using-Halvade-framework-on-Amazon-EMR